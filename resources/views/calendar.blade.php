@extends('layouts.cal')
@section('content')


<!-- Intro Header -->
<header class="masthead">
    <div class="intro-body">
	<section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="brand-heading">Selamat Datang</h1>
                    <p class="intro-text">Di Sistem Informasi Administrasi Departemen Ilmu Komputer/Informatika
                        <h3>Anda Butuh Ruang?</h3>
                    <a href="#download" class="btn btn-circle page-scroll">
                        <i class="fa fa-angle-double-down animated"></i>
                    </a>
                </div>
            </div>
        </div>
		</section>
    </div>
</header>

<!-- Modal -->
<div class="modal fade" id="filterModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Kalender Peminjaman Ruang</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div>
                        {!! $calendar->calendar() !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                Keterangan:<span> </span>
                <span class="colorBox red-1"></span>R. Sidang (Disetujui)<span> </span>
                <span class="colorBox red-2"></span>R. Sidang (Dipesan)<span> </span>
                <span class="colorBox green-1"></span>Lab (Disetujui)<span> </span>
                <span class="colorBox green-2"></span>Lab (Dipesan)
            </div>
        </div>
    </div>
</div>

<!-- Download Section -->
<section id="download" class="text-center">
    <div class="download-section">
        <div class="container">
            <div class="col-lg-12">
                <h2>Peminjaman Ruang</h2>
                <div class="col-md-8 offset-md-2">
                    <p>Ingin meminjam Ruang Sidang atau Laboratorium di Departemen Ilmu Komputer/Informatika Undip?</p>
                    <div class="col-md-8 offset-md-2">

                    <div>
                    <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#filterModal">Lihat Kalender</button>
                    <a href="#contact" class="btn btn-default btn-lg">Pinjam Ruangan</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact" class="container content-section">
    <div class="row">
        <div class="col-lg-8 offset-md-2">
            <h2>Masukkan Data Peminjaman</h2>
            <form action="{{ route('calendar_events.store') }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group{{ $errors->has('namaRuang') ? ' has-warning' : '' }}">
                    <label for="namaRuang">Ruang</label></br>
                    <input type="radio" name="namaRuang"  value="Ruang Sidang" onClick="document.getElementById('bg').value='#EF9A9A'" checked> Ruang Sidang
                    <input type="radio" name="namaRuang"  value="Laboratorium" onClick="document.getElementById('bg').value='#81C784'"> Laboratorium<br>
                    {!! $errors->first('namaRuang', '<p class="help-block"><font color="#f7541d"> :message </font></p>') !!}
                </div>
                <div class="form-group{{ $errors->has('tanggalPakai') ? ' has-warning' : '' }}">
                    <label for="tanggalPakai">Tanggal Pemakaian</label>
                    <input type="date" name="tanggalPakai" @if($errors->has('tanggalPakai')) autofocus @endif class="form-control" placeholder="Tanggal Pemakaian" @if(Session::has('tanggal')) value = {{ Session::get('tanggal') }} @endif value="{{ old('tanggalPakai') }}">
                    {!! $errors->first('tanggalPakai', '<p class="help-block"><font color="#f7541d"> :message </font></p>') !!}
                </div>
                <div class="form-group{{ $errors->has('kegiatan') ? ' has-warning' : '' }}">
                    <label for="kegiatan">Kegiatan</label>
                    <input type="text" name="kegiatan" @if($errors->has('kegiatan')) autofocus @endif class="form-control" placeholder="Kegiatan" @if(Session::has('tanggal')) value = {{ Session::get('kegiatan') }} @endif value="{{ old('kegiatan') }}" >
                    {!! $errors->first('kegiatan', '<p class="help-block"><font color="#f7541d"> :message </font></p>') !!}
                </div>
                <div class="form-group{{ $errors->has('namaPeminjam') ? ' has-warning' : '' }}">
                    <label for="namaPeminjam">Nama Peminjam</label>
                    <input type="text" pattern='[ .a-zA-Z]{4,}' title="Minimal 4 Huruf.." name="namaPeminjam" @if($errors->has('namaPeminjam')) autofocus @endif class="form-control" placeholder="Nama @if(Session::has('tanggal')) value = {{ Session::get('nama') }} @endif Peminjam" value="{{ old('namaPeminjam') }}">
                    {!! $errors->first('namaPeminjam', '<p class="help-block"><font color="#f7541d"> :message </font></p>') !!}
                </div>
                <div class="form-group{{ $errors->has('kontak') ? ' has-warning' : '' }}">
                    <label for="kontak">Kontak</label>
                    <input type="text" pattern='[0-9]{10,13}' title="Masukkan Nomor Telepon yang Benar.." name="kontak" @if($errors->has('kontak')) autofocus @endif class="form-control" placeholder="Kontak" @if(Session::has('tanggal')) value = {{ Session::get('kontak') }} @endif value="{{ old('kontak') }}">
                    {!! $errors->first('kontak', '<p class="help-block"><font color="#f7541d"> :message </font></p>') !!}
                </div>
                <div class="form-group{{ $errors->has('jamMulai') ? ' has-warning' : '' }}{{ Session::has('alert-success') ? ' has-warning' : '' }}">
                    <label for="jamMulai">Jam Mulai</label>
                    <input type="time" name="jamMulai" @if($errors->has('jamMulai') || (Session::has('tanggal'))) autofocus @endif class="form-control" placeholder="Jam Mulai" @if(Session::has('tanggal')) value = {{ Session::get('mulai') }} @endif value="{{ old('jamMulai') }}" step="3600">
                    {!! $errors->first('jamMulai', '<p class="help-block"><font color="#f7541d"> :message </font></p>') !!}
					@if(Session::has('alert-success'))
						<p class="help-block"><font color="#f7541d"> {{ Session::get('alert-success') }}  </font></p>
					@endif
                </div>
                <div class="form-group{{ $errors->has('jamSelesai') ? ' has-warning' : '' }}{{ Session::has('alert-success') ? ' has-warning' : '' }}">
                    <label for="jamSelesai">Jam Selesai</label>
                    <input type="time" name="jamSelesai" @if($errors->has('jamSelesai')) autofocus @endif class="form-control" placeholder="Jam Selesai" @if(Session::has('tanggal')) value = {{ Session::get('selesai') }} @endif value="{{ old('jamSelesai') }}" step="3600">
                    {!! $errors->first('jamSelesai', '<p class="help-block"><font color="#f7541d"> :message </font></p>') !!}
					@if(Session::has('alert-success'))
						<p class="help-block"><font color="#f7541d"> {{ Session::get('alert-success') }}  </font></p>
					@endif
                </div>
                <div class="form-group{{ $errors->has('lampiran') ? ' has-warning' : '' }}">
                    <label for="upload">Lampiran</label></br>
                    <input type="file" name="file" id="file" value=""></input>
                    {!! $errors->first('lampiran', '<p class="help-block"><font color="#f7541d"> :message </font></p>') !!}
                </div></br>
                <input type="hidden" name="status" class="form-control" value="Dipesan"/>
                <input type="hidden" name="is_all_day" class="form-control" value="0"/>
                <input type="hidden" name="background_color" id="bg" class="form-control" value=""/>
            <button class="btn btn-default" type="submit" >Simpan</button>
            </form>
        </div>
    </div>
</section>
@stop


@section('scripts')
    {!! $calendar->script() !!}
@stop
