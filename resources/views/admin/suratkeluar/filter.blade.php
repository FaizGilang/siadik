@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">

			<div class="row">
				<div class="col-md-2">
					<h3>Surat Keluar</h3>
				</div>
				<div class="col-md-10">
					<a href="{{ url('/admin/suratkeluar/create') }}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus"></span> Tambah Data</a>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					@if(Session::has('alert-success'))
					    <div class="alert alert-success">
				            {{ Session::get('alert-success') }}
				        </div>
					@endif

					<div class="row">
						<div class="col-md-1">
                            	<button type="button" class="btn btn-default" data-toggle="modal" data-target="#filterModal"><span class="glyphicon glyphicon-filter"></span> Saring</button>
                        </div>
						<div class="col-md-4">
							<div class="col-md-12">
								<form method="GET" action="{{ url('/admin/suratkeluar/rekap') }}" target="_blank">
									<input type="hidden" name="bulan" value="{{ $surat_keluars->bulan }}">
									<input type="hidden" name="tahun" value="{{ $surat_keluars->tahun }}">
										<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-download"></span> Rekapitulasi</button>
								</form>
							</div>
                        </div>
                        <div class="col-md-6">
							<form method="GET" action="{{ url('/admin/suratkeluar/search') }}">
								<div class="form-group col-md-10">
									<input type="text" name="s" class="form-control" placeholder="Cari perihal surat">
								</div>
								<div class="form-group">
									<button class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Cari</button>
								</div>
							</form>
                        </div>
                    </div>

					<table class="table table-bordered">
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center;">Nomor</th>
							<th style="text-align:center;">Penerima</th>
							<th style="text-align:center;">Tanggal Keluar</th>
							<th style="text-align:center;  word-wrap:break-word;">Perihal</th>
							<th style="text-align:center;">Lampiran</th>
							<th style="text-align:center;">Action</th>
						</tr>
						<?php $no=1; ?>
						@foreach($surat_keluars->sortBy('tanggal') as $suratkeluars)
						<tr>
							<td>{{$no++}}</td>
							<td>{{$suratkeluars->nomor}}</td>
							<td>{{$suratkeluars->penerima}}</td>
							<td>{{$suratkeluars->tanggal}}</td>
							<td style="max-width: 180px;">{{$suratkeluars->perihal}}</td>
							<td class="text-center">
								<a class="btn btn-primary" target="_blank" href={{ URL::asset("uploads/{$suratkeluars->gambar}")}} <?php echo ($suratkeluars->gambar=='')?'disabled':'' ?>>Lihat</a>
							</td>
							<td class="text-center">
								<div class="dropdown">
									<a href="#" class="dropdown-toggle btn btn-primary" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-chevron-down"></span></a>
									<ul class="dropdown-menu">
										<li><a data-toggle="modal" data-id="#" class="openEditObat" href="{{url('/admin/suratkeluar/edit/'.$suratkeluars->id)}}" ><span class="glyphicon glyphicon-pencil"></span> Ubah</a></li>
									</ul>
								</div>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="filterModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Saring Berdasarkan Bulan & Tahun</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
					<form method="GET" action="{{ url('/admin/suratkeluar/filter') }}">
						<div class="form-group">
							<label for="bulan">Bulan</label>
							<select name="bulan" class="form-control">
								<option value='00'>-</option>
								<option value='01'>Januari</option>
								<option value='02'>Februari</option>
								<option value='03'>Maret</option>
								<option value='04'>April</option>
								<option value='05'>Mei</option>
								<option value='06'>Juni</option>
								<option value='07'>Juli</option>
								<option value='08'>Agustus</option>
								<option value='09'>September</option>
								<option value='10'>Oktober</option>
								<option value='11'>November</option>
								<option value='12'>Desember</option>
							</select>
						</div>
						<div class="form-group">
							<label for="tahun">Tahun</label>
							<input type="number" class="form-control" name="tahun" placeholder="Tahun" min="2000">
						</div>
						<div class="form-group">
							<button class="btn btn-primary">Saring</button>
						</div>
					</form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
