@extends('layouts.rekap')
@section('content')
<body onload="window.print()">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			@if(Session::has('alert-success'))
			    <div class="alert alert-success">
		            {{ Session::get('alert-success') }}
		        </div>
			@endif
			<h3 class="text-center" style="font-weight: bold;">Rekapitulasi Surat Masuk</h3>
			@if($surat_keluars->bulan !== '00')
				<h4 class="text-center" style="font-weight: bold;">Bulan
				@if ($surat_keluars->bulan === '01')
					Januari
				@elseif($surat_keluars->bulan === '02')
					Februari
				@elseif($surat_keluars->bulan === '03')
					Maret
				@elseif($surat_keluars->bulan === '04')
					April
				@elseif($surat_keluars->bulan === '05')
					Mei
				@elseif($surat_keluars->bulan === '06')
					Juni
				@elseif($surat_keluars->bulan === '07')
					Juli
				@elseif($surat_keluars->bulan === '08')
					Agustus
				@elseif($surat_keluars->bulan === '09')
					September
				@elseif($surat_keluars->bulan === '10')
					Oktober
				@elseif($surat_keluars->bulan === '11')
					November
				@elseif($surat_keluars->bulan === '12')
					Desember
				@endif
				</h4>
			@endif
			<h4 class="text-center" style="font-weight: bold;">
			Tahun {{$surat_keluars->tahun}}
			</h4>
			</br>

			<table class="table table-bordered">
				<tr>
					<th style="text-align:center;">No</th>
					<th style="text-align:center;">Nomor</th>
					<th style="text-align:center;">Penerima</th>
					<th style="text-align:center; white-space: nowrap;">Tanggal Keluar</th>
					<th style="text-align:center;">Perihal</th>
				</tr>
				<?php $no=1; ?>
				@foreach($surat_keluars->sortBy('tanggal') as $suratkeluars)
				<tr>
					<td>{{$no++}}</td>
					<td>{{$suratkeluars->nomor}}</td>
					<td>{{$suratkeluars->penerima}}</td>
					<td>{{$suratkeluars->tanggal}}</td>
					<td>{{$suratkeluars->perihal}}</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
</body>
@endsection
