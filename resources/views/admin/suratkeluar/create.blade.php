@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h3>Tambah Surat Keluar</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<form enctype="multipart/form-data" action="{{ url('/admin/suratkeluar/store')}}" method="post">
					{{csrf_field()}}
						<div class="form-group{{ $errors->has('nomor') ? ' has-error' : '' }}">
							<label for="nama">Nomor Surat</label>
							<input type="text" name="nomor" class="form-control" placeholder="Nomor Surat" value="{{ old('nomor') }}">
							{!! $errors->first('nomor', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('penerima') ? ' has-error' : '' }}">
							<label for="penerima">Penerima</label>
							<input type="text" pattern='[a-zA-Z ]{4,}' title="Minimal 4 Huruf.." name="penerima" class="form-control" placeholder="Penerima Surat" value="{{ old('penerima') }}">
							{!! $errors->first('penerima', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
							<label for="tanggal">Tanggal</label>
							<input type="date" name="tanggal" class="form-control" value="{{ old('tanggal') }}">
							{!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('perihal') ? ' has-error' : '' }}">
							<label for="perihal">Perihal</label>
							<textarea name="perihal" class="form-control" placeholder="Perihal Surat" >{{ old('perihal') }}</textarea>
							{!! $errors->first('perihal', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
							<label for="upload">Lampiran</label>
							<input type="file" name="file" id="file"></input>
							{!! $errors->first('gambar', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group">
							<input type="hidden" value="{{csrf_token()}}" name="_token">
							<input type="submit" class="btn btn-primary" value="Simpan">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
