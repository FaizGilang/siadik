@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h3>Tambah Peminjaman Ruang Sidang</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<form enctype="multipart/form-data" action="{{ url('/admin/peminjaman/store')}}" method="post">
					{{csrf_field()}}
						<input type="hidden" name="namaRuang"  value="Ruang Sidang">
						<input type="hidden" name="is_all_day"  value="0">
						<input type="hidden" name="background_color"  value="">
						<div class="form-group{{ $errors->has('tanggalPakai') ? ' has-error' : '' }}">
							<label for="tanggalPakai">Tanggal Pemakaian</label>
							<input type="date" name="tanggalPakai" class="form-control" placeholder="Tanggal Pemakaian" @if(Session::has('tanggal'))
							value = {{ Session::get('tanggal') }}
							@endif value="{{ old('tanggalPakai') }}">
							{!! $errors->first('tanggalPakai', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('kegiatan') ? ' has-error' : '' }}">
							<label for="kegiatan">Kegiatan</label>
							<input type="text" name="kegiatan" class="form-control" placeholder="Kegiatan" @if(Session::has('tanggal'))
							value = {{ Session::get('kegiatan') }}
							@endif value="{{ old('kegiatan') }}">
							{!! $errors->first('kegiatan', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('namaPeminjam') ? ' has-error' : '' }}">
							<label for="namaPeminjam">Nama Peminjam</label>
							<input type="text" pattern='[ .a-zA-Z]{4,}' title="Minimal 4 Huruf.." name="namaPeminjam" class="form-control" placeholder="Nama Peminjam" @if(Session::has('tanggal'))
							value = {{ Session::get('nama') }}
							@endif value="{{ old('namaPeminjam') }}">
							{!! $errors->first('namaPeminjam', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('kontak') ? ' has-error' : '' }}">
							<label for="kontak">Kontak</label>
							<input type="text" pattern='[0-9]{10,13}' title="Masukkan Nomor Telepon yang Benar.." name="kontak" class="form-control" placeholder="Kontak" @if(Session::has('tanggal'))
							value = {{ Session::get('kontak') }}
							@endif value="{{ old('kontak') }}">
							{!! $errors->first('kontak', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('jamMulai') ? ' has-error' : '' }}{{ Session::has('alert-success') ? ' has-error' : '' }}">
							<label for="jamMulai">Jam Mulai</label>
							<input type="time" name="jamMulai" class="form-control" placeholder="Jam Mulai" @if(Session::has('tanggal'))
							value = {{ Session::get('mulai') }}
							@endif value="{{ old('jamMulai') }}" step="3600">
							{!! $errors->first('jamMulai', '<p class="help-block">:message</p>') !!}
							@if(Session::has('alert-success'))
								<p class="help-block">
							{{ Session::get('alert-success') }}
								</p>
							@endif
						</div>
						<div class="form-group{{ $errors->has('jamSelesai') ? ' has-error' : '' }}{{ Session::has('alert-success') ? ' has-error' : '' }}">
							<label for="jamSelesai">Jam Selesai</label>
							<input type="time" name="jamSelesai" class="form-control" placeholder="Jam Selesai" @if(Session::has('tanggal'))
							value = {{ Session::get('selesai') }}
							@endif value="{{ old('jamSelesai') }}" step="3600">
							{!! $errors->first('jamSelesai', '<p class="help-block">:message</p>') !!}@if(Session::has('alert-success'))
								<p class="help-block">
							{{ Session::get('alert-success') }}
								</p>
							@endif
						</div>
						<div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
							<label for="upload">Lampiran</label>
							<input type="file" name="file" id="file"></input>
							{!! $errors->first('gambar', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
							<label for="status">Status</label>
							<input type="radio" name="status"  value="Dipesan" checked> Dipesan<br>
							<input type="radio" name="status"  value="Disetujui"> Disetujui<br>
							{!! $errors->first('status', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Simpan">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
