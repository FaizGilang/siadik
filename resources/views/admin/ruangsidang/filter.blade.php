@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-5">
					<h3>Peminjaman Ruang Sidang</h3>
				</div>
				<div class="col-md-7">
					<a href="{{ url('/admin/peminjaman/ruangsidang/create') }}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus"></span> Tambah Data</a><br><br>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					@if(Session::has('alert-success'))
						<div class="alert alert-success">
							{{ Session::get('alert-success') }}
						</div>
					@endif

					<div class="row">
						<div class="col-md-1">
                            	<button type="button" class="btn btn-default" data-toggle="modal" data-target="#filterModal"><span class="glyphicon glyphicon-filter"></span> Saring</button>
                        </div>
						<div class="col-md-4">
							<div class="col-md-12">
								<form method="GET" action="{{ url('/admin/peminjaman/ruangsidang/rekap') }}" target="_blank">
									<input type="hidden" name="bulan" value="{{ $peminjamans->bulan }}">
									<input type="hidden" name="tahun" value="{{ $peminjamans->tahun }}">
										<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-download"></span> Rekapitulasi</button>
								</form>
							</div>
                        </div>
                        <div class="col-md-6">
							<form method="GET" action="{{ url('/admin/peminjaman/ruangsidang/search') }}">
								<div class="form-group col-md-10">
									<input type="text" name="s" class="form-control" placeholder="Cari perihal surat">
								</div>
								<div class="form-group">
									<button class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Cari</button>
								</div>
							</form>
                        </div>
                    </div>

					<table class="table table-bordered">
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center;">Tanggal Pemakaian</th>
							<th style="text-align:center;">Kegiatan</th>
							<th style="text-align:center;">Nama Peminjam</th>
							<th style="text-align:center;">Kontak</th>
							<th style="text-align:center;">Jam Mulai</th>
							<th style="text-align:center;">Jam Selesai</th>
							<th style="text-align:center;">Lampiran</th>
							<th style="text-align:center;">Status</th>
							<th style="text-align:center;">Pilihan</th>
						</tr>
						<?php $no=1; ?>
						@foreach($peminjamans->sortBy('id') as $peminjamans)
						<tr>
							<td>{{$no++}}</td>
							<td>{{$peminjamans->tanggalPakai}}</td>
							<td>{{$peminjamans->kegiatan}}</td>
							<td>{{$peminjamans->namaPeminjam}}</td>
							<td>{{$peminjamans->kontak}}</td>
							<td>{{$peminjamans->jamMulai}}</td>
							<td>{{$peminjamans->jamSelesai}}</td>
							<td class="text-center">
								<a class="btn btn-primary" target="_blank" href={{ URL::asset("uploads/{$peminjamans->lampiran}")}} <?php echo ($peminjamans->lampiran=='')?'disabled':'' ?>>Lihat</a>
							</td>
							<td>{{$peminjamans->status}}</td>
							<td class="text-center">
								<div class="dropdown">
									<a href="#" class="dropdown-toggle btn btn-primary" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-chevron-down"></span></a>
									<ul class="dropdown-menu">
										<li><a data-toggle="modal" data-id="#" class="openEditObat" href="{{url('/admin/peminjaman/ruangsidang/approve/'.$peminjamans->id)}}" ><span class="glyphicon glyphicon-thumbs-up"></span> Setujui</a></li>
										<li><a data-toggle="modal" data-id="#" class="openEditObat" href="{{url('/admin/peminjaman/ruangsidang/edit/'.$peminjamans->id)}}" ><span class="glyphicon glyphicon-pencil"></span> Ubah</a></li>
										<li><a data-toggle="modal" data-id="#" class="openDeleteObat" href="{{url('/admin/peminjaman/ruangsidang/delete/'.$peminjamans->id)}}"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
									</ul>
								</div>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="filterModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Saring Berdasarkan Bulan & Tahun</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
					<form method="GET" action="{{ url('/admin/peminjaman/ruangsidang/filter') }}">
						<div class="form-group">
							<label for="bulan">Bulan</label>
							<select name="bulan" class="form-control">
								<option value='00'>-</option>
								<option value='01'>Januari</option>
								<option value='02'>Februari</option>
								<option value='03'>Maret</option>
								<option value='04'>April</option>
								<option value='05'>Mei</option>
								<option value='06'>Juni</option>
								<option value='07'>Juli</option>
								<option value='08'>Agustus</option>
								<option value='09'>September</option>
								<option value='10'>Oktober</option>
								<option value='11'>November</option>
								<option value='12'>Desember</option>
							</select>
						</div>
						<div class="form-group">
							<label for="tahun">Tahun</label>
							<input type="number" class="form-control" name="tahun" placeholder="Tahun" min="2000">
						</div>
						<div class="form-group">
							<button class="btn btn-primary">Saring</button>
						</div>
					</form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
					<div class="form-group">
						Apakah anda yakin ingin menghapus data ini?
					</div>
					<div class="form-group">
						<a  href="{{url('/admin/peminjaman/ruangsidang/delete/')}}"><button type="button" class="btn btn-primary" >Hapus</button></a>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
