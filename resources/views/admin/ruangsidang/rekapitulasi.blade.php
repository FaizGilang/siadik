@extends('layouts.rekap')
@section('content')
<body onload="window.print()">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
					@if(Session::has('alert-success'))
					    <div class="alert alert-success">
				            {{ Session::get('alert-success') }}
				        </div>
					@endif
					<h3 class="text-center" style="font-weight: bold;">Rekapitulasi Surat Masuk</h3>
					@if($peminjamans->bulan !== '00')
						<h4 class="text-center" style="font-weight: bold;">Bulan
						@if ($peminjamans->bulan === '01')
							Januari
						@elseif($peminjamans->bulan === '02')
							Februari
						@elseif($peminjamans->bulan === '03')
							Maret
						@elseif($peminjamans->bulan === '04')
							April
						@elseif($peminjamans->bulan === '05')
							Mei
						@elseif($peminjamans->bulan === '06')
							Juni
						@elseif($peminjamans->bulan === '07')
							Juli
						@elseif($peminjamans->bulan === '08')
							Agustus
						@elseif($peminjamans->bulan === '09')
							September
						@elseif($peminjamans->bulan === '10')
							Oktober
						@elseif($peminjamans->bulan === '11')
							November
						@elseif($peminjamans->bulan === '12')
							Desember
						@endif
						</h4>
					@endif
					<h4 class="text-center" style="font-weight: bold;">
					Tahun {{$peminjamans->tahun}}
					</h4>

					</br>
					<table class="table table-bordered">
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center; white-space: nowrap;">Tanggal Pemakaian</th>
							<th style="text-align:center;">Kegiatan</th>
							<th style="text-align:center;">Nama Peminjam</th>
							<th style="text-align:center;">Kontak</th>
							<th style="text-align:center;">Jam Mulai</th>
							<th style="text-align:center;">Jam Selesai</th>
							<th style="text-align:center;">Status</th>
						</tr>
						<?php $no=1; ?>
						@foreach($peminjamans->sortBy('tanggalPakai') as $peminjaman)
						<tr>
							<td>{{$no++}}</td>
							<td>{{$peminjaman->tanggalPakai}}</td>
							<td>{{$peminjaman->kegiatan}}</td>
							<td>{{$peminjaman->namaPeminjam}}</td>
							<td>{{$peminjaman->kontak}}</td>
							<td>{{$peminjaman->jamMulai}}</td>
							<td>{{$peminjaman->jamSelesai}}</td>
							<td>{{$peminjaman->status}}</td>
						</tr>
						@endforeach
					</table>
		</div>
	</div>
</div>
</body>
@endsection
