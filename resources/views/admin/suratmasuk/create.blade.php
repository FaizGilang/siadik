@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h3>Tambah Surat Masuk</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<form enctype="multipart/form-data" action="{{ url('/admin/suratmasuk/store')}}" method="post">
					{{csrf_field()}}
						<div class="form-group{{ $errors->has('nomor') ? ' has-error' : '' }}">
							<label for="nomor">Nomor Surat</label>
							<input type="text" name="nomor" class="form-control" placeholder="Nomor Surat" value="{{ old('nomor') }}" size="2">
							{!! $errors->first('nomor', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('hp') ? ' has-error' : '' }}">
							<label for="hp">Nomor Handphone</label>
							<input type="text" pattern='[0-9]{10,13}' title="Masukkan Nomor Telepon yang Benar.." name="hp" class="form-control" placeholder="Nomor Handphone" value="{{ old('hp') }}">
							{!! $errors->first('hp', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('pengirim') ? ' has-error' : '' }}">
							<label for="pengirim">Pengirim</label>
							<input type="text" pattern='[a-zA-Z ]{4,}' title="Minimal 4 Huruf.." name="pengirim" class="form-control" placeholder="Pengirim Surat" value="{{ old('pengirim') }}">
							{!! $errors->first('pengirim', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
							<label for="tanggal">Tanggal</label>
							<input type="date" name="tanggal" class="form-control" placeholder="Tanggal Masuk" value="{{ old('tanggal') }}">
							{!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('perihal') ? ' has-error' : '' }}">
							<label for="perihal">Perihal</label>
							<textarea name="perihal" class="form-control" placeholder="Perihal Surat" >{{ old('perihal') }}</textarea>
							{!! $errors->first('perihal', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
							<label for="upload">Lampiran</label>
							<input type="file" name="file" id="file"></input>
							{!! $errors->first('gambar', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group">
							<input type="hidden" value="{{csrf_token()}}" name="_token">
							<input type="submit" class="btn btn-primary" value="Simpan">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
