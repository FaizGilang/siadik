@extends('layouts.rekap')
@section('content')
<body onload="window.print()">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			@if(Session::has('alert-success'))
			    <div class="alert alert-success">
		            {{ Session::get('alert-success') }}
		        </div>
			@endif
			<h3 class="text-center" style="font-weight: bold;">Rekapitulasi Surat Masuk</h3>
			@if($surat_masuks->bulan !== '00')
				<h4 class="text-center" style="font-weight: bold;">Bulan
				@if ($surat_masuks->bulan === '01')
					Januari
				@elseif($surat_masuks->bulan === '02')
					Februari
				@elseif($surat_masuks->bulan === '03')
					Maret
				@elseif($surat_masuks->bulan === '04')
					April
				@elseif($surat_masuks->bulan === '05')
					Mei
				@elseif($surat_masuks->bulan === '06')
					Juni
				@elseif($surat_masuks->bulan === '07')
					Juli
				@elseif($surat_masuks->bulan === '08')
					Agustus
				@elseif($surat_masuks->bulan === '09')
					September
				@elseif($surat_masuks->bulan === '10')
					Oktober
				@elseif($surat_masuks->bulan === '11')
					November
				@elseif($surat_masuks->bulan === '12')
					Desember
				@endif
				</h4>
			@endif
			<h4 class="text-center" style="font-weight: bold;">
			Tahun {{$surat_masuks->tahun}}
			</h4>
			</br>
			<table class="table table-bordered">
				<tr>
					<th style="text-align:center;">No</th>
					<th style="text-align:center;">Nomor</th>
					<th style="text-align:center;">Pengirim</th>
					<th style="text-align:center;">No HP</th>
					<th style="text-align:center; white-space: nowrap;">Tanggal Masuk</th>
					<th style="text-align:center;">Perihal</th>
				</tr>
				<?php $no=1; ?>
				@foreach($surat_masuks->sortBy('tanggal') as $suratmasuks)
				<tr>
					<td>{{$no++}}</td>
					<td>{{$suratmasuks->nomor}}</td>
					<td>{{$suratmasuks->pengirim}}</td>
					<td>{{$suratmasuks->hp}}</td>
					<td>{{$suratmasuks->tanggal}}</td>
					<td>{{$suratmasuks->perihal}}</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
</body>
@endsection
