<?php

include('php/connect.php'); 

if(isset($_POST['slots_booked'])) $slots_booked = mysqli_real_escape_string($link, $_POST['slots_booked']);
if(isset($_POST['name'])) $name = mysqli_real_escape_string($link, $_POST['name']);
if(isset($_POST['email'])) $email = mysqli_real_escape_string($link, $_POST['email']);
if(isset($_POST['phone'])) $phone = mysqli_real_escape_string($link, $_POST['phone']);
if(isset($_POST['nim'])) $nim = mysqli_real_escape_string($link, $_POST['nim']);
if(isset($_POST['booking_date'])) $booking_date = mysqli_real_escape_string($link, $_POST['booking_date']);
$kode= mysqli_real_escape_string($link, $_POST['nim']).mysqli_real_escape_string($link, $_POST['booking_date']);


$explode = explode('|', $slots_booked);

foreach($explode as $slot) {

	if(strlen($slot) > 0) {

		$stmt = $link->prepare("INSERT INTO bookings (date, start, name,nim, email, phone,kode) VALUES (?, ?, ?, ?, ?,?,?)"); 
		$stmt->bind_param('sssssss', $booking_date, $slot, $name,$nim, $email, $phone,$kode);
		$stmt->execute();
		
	} // Close if
	
} // Close foreach


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Booking Confirmed</title>
<link href="style.css" rel="stylesheet" type="text/css">

<link href="http://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>

</head>

<body>
<script type="text/javascript">
    window.location.replace('http://localhost:8000/admin/suratmasuk');
</script>
</body>

</html>
