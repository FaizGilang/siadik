@extends('layouts.cal')

@section('content')
    <div class="page-header">
        <h1>CalendarEvents / Create </h1>
    </div>


    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('calendar_events.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group{{ $errors->has('namaRuang') ? ' has-error' : '' }}">
                    <label for="status">Ruang</label>
                    <input type="radio" name="namaRuang"  value="Ruang Sidang" onClick="document.getElementById('bg').value='#ff0000'"> Ruang Sidang<br>
                    <input type="radio" name="namaRuang"  value="Laboratorium" onClick="document.getElementById('bg').value='#0000ff'"> Laboratorium<br>
                    {!! $errors->first('namaRuang', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('tanggalPakai') ? ' has-error' : '' }}">
                    <label for="tanggalPakai">Tanggal Pemakaian</label>
                    <input type="date" name="tanggalPakai" class="form-control" placeholder="Tanggal Pemakaian" value="{{ old('tanggal') }}">
                    {!! $errors->first('tanggalPakai', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('kegiatan') ? ' has-error' : '' }}">
                    <label for="kegiatan">Kegiatan</label>
                    <input type="text" name="kegiatan" class="form-control" placeholder="Kegiatan" value="{{ old('penerima') }}">
                    {!! $errors->first('kegiatan', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('namaPeminjam') ? ' has-error' : '' }}">
                    <label for="namaPeminjam">Nama Peminjam</label>
                    <input type="text" name="namaPeminjam" class="form-control" placeholder="Nama Peminjam" value="{{ old('penerima') }}">
                    {!! $errors->first('namaPeminjam', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('kontak') ? ' has-error' : '' }}">
                    <label for="kontak">Kontak</label>
                    <input type="number" name="kontak" class="form-control" placeholder="Kontak" value="{{ old('penerima') }}">
                    {!! $errors->first('kontak', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('jamMulai') ? ' has-error' : '' }}">
                    <label for="jamMulai">Jam Mulai</label>
                    <input type="time" name="jamMulai" class="form-control" placeholder="Jam Mulai" value="{{ old('tanggal') }}" step="3600">
                    {!! $errors->first('jamMulai', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('jamSelesai') ? ' has-error' : '' }}">
                    <label for="jamSelesai">Jam Selesai</label>
                    <input type="time" name="jamSelesai" class="form-control" placeholder="Jam Selesai" value="{{ old('tanggal') }}" step="3600">
                    {!! $errors->first('jamSelesai', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('lampiran') ? ' has-error' : '' }}">
                    <label for="upload">Lampiran</label>
                    <input type="file" name="file" id="file"></input>
                    {!! $errors->first('lampiran', '<p class="help-block">:message</p>') !!}
                </div>
                <input type="hidden" name="status" class="form-control" value="Dipesan"/>
                <input type="hidden" name="is_all_day" class="form-control" value="0"/>
                <input type="hidden" name="background_color" id="bg" class="form-control" value=""/>
            <a class="btn btn-default" href="{{ route('calendar_events.index') }}">Back</a>
            <button class="btn btn-primary" type="submit" >Create</button>
            </form>
        </div>
    </div>

@endsection
