<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sistem Informasi Administrasi Departemen Ilmu Komputer/Informatika</title>
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
	<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('css/dashboard.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('css/font-awesome/css/font-awesome.min.css') }}" />
</head>
<body>
    <header class="container-fluid zhm-navbar">

      <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Sistem Informasi Administrasi <b>Departemen Ilmu Komputer/Informatika</b></a>
          </div>
        </div>
      </nav>
    </header>

    <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#project"><i class="fa fa-fw fa fa-th-list"></i> Peminjaman Ruang Sidang <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="project" class="collapse">
                            <li>
                                <a href="{{ url('/admin/project') }}">Ruang Sidang I</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/project/create') }}">Ruang Sidang II</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#quotation"><i class="fa fa-fw fa-th-list"></i> Peminjaman Laboratorium <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="quotation" class="collapse">
                            <li>
                                <a href="{{ url('/admin/quotation') }}">Laboratorium B</a>
                            </li>
                            <li>
                                <a href="/admin/quotation/add">Laboratorium E</a>
                            </li>
                        </ul>
                    </li>
                </ul>
    </div>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		@yield('content')
	</div>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

</body>
</html>
