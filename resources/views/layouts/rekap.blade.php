<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sistem Informasi Administrasi Departemen Ilmu Komputer/Informatika</title>
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
	<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('css/dashboard.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}" />
</head>
<body>
	<div class="col-sm-9 col-sm-offset-3 col-md-8 col-md-offset-2">
		@yield('content')
	</div>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

</body>
</html>
