	<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistem Informasi Admnisitrasi Departemen Ilmu Komputer/Informatika</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<!-- <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">-->
		<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
		<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

    <!-- Custom fonts for this template -->
    <link href="{{ URL::asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('css/grayscale.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('css/fullcalendar.min.css') }}"/>
    <link rel="stylesheet" href="{{ URL::asset('css/fullcalendar.print.css') }}" media="print"/>

    <!-- Temporary navbar container fix -->
    <style>
    .navbar-toggler {
        z-index: 1;
    }

    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }

    .colorBox {
      width: 20px;
      height: 20px;
      margin: 5px;
      border: 1px solid rgba(0, 0, 0, .2);
    }

    .red-1 {
      background: #B71C1C;
    }

	.red-2 {
      background: #EF9A9A;
    }

	.green-1 {
      background: #1B5E20;
    }

	.green-2 {
      background: #81C784;
    }

    </style>

</head>

<body id="page-top">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar fixed-top navbar-toggleable-md navbar-light">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            Menu <i class="fa fa-bars"></i>
        </button>
        <div class="container">
			<img src="{{ URL::asset('images/Siadik.png') }}" width="30" height="30" class="img-responsive">&nbsp
            <a class="navbar-brand" href="#page-top">SIADIK</a>
            <div class="collapse navbar-collapse" id="navbarExample">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#download">Peminjaman Ruang</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Masukkan Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    @yield('content')

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; Informatika 2017</p>
        </div>
    </footer>


    <!-- Bootstrap core JavaScript -->
    <script src="{{ URL::asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/tether/tether.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ URL::asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ URL::asset('js/grayscale.min.js') }}"></script>

    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
    <script src="{{ URL::asset('js/moment.min.js') }}"></script>
    <script src="{{ URL::asset('js/fullcalendar.min.js') }}"></script>
    @yield('scripts')

</body>

</html>
