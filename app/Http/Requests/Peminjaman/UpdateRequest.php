<?php

namespace App\Http\Requests\peminjaman;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules(Request $request)
    {
        return [
			'tanggalPakai'=> 'Required',
			'kegiatan'=> 'Required',
            'namaPeminjam'=> 'Required',
			'kontak'=> 'Required',
            'jamMulai'=> 'Required',
			'jamSelesai'=> 'Required',
        ];
    }

    public function messages()
    {
        return [
			'tanggalPakai.required' => 'Tanggal Pemakaian Ruang Tidak Boleh Kosong.',
			'kegiatan.required' => 'Kegiatan Tidak Boleh Kosong.',
			'namaPeminjam.required' => 'Nama Peminjam Ruang Tidak Boleh Kosong.',
            'kontak.required' => 'Kontak Peminjam Ruang Tidak Boleh Kosong.',
			'jamMulai.required' => 'Jam Mulai Pemakaian Ruang Tidak Boleh Kosong.',
			'jamSelesai.required' => 'Jam Selesai Pemakaian Ruang Tidak Boleh Kosong.',
        ];
    }
}
