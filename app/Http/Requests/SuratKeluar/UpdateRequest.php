<?php

namespace App\Http\Requests\suratkeluar;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules(Request $request)
    {
        return [
			'nomor'=> 'Required:surat_keluars,nomor',
			'penerima'=> 'Required:surat_keluars,penerima',
			'tanggal'=> 'Required:surat_keluars,tanggal',
			'perihal'=> 'Required:surat_keluars,perihal',
        ];
    }

    public function messages()
    {
        return [
			'nomor.required' => 'Nomor Surat Tidak Boleh Kosong.',
            'penerima.required' => 'Penerima Tidak Boleh Kosong.',
			'tanggal.required' => 'Tanggal Tidak Boleh Kosong.',
			'perihal.required' => 'Perihal Surat Tidak Boleh Kosong.',
        ];
    }
}
