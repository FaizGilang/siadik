<?php

namespace App\Http\Requests\calender;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'tanggalPakai'=> 'Required',
			'kegiatan'=> 'Required',
			'namaPeminjam'=> 'Required',
			'kontak'=> 'Required',
			'jamMulai'=> 'Required',
			'jamSelesai'=> 'Required',
        ];
    }

    public function messages()
    {
        return [
            'tanggalPakai.required'=> 'Tanggal Tidak Boleh Kosong.',
			'kegiatan.required'=> 'Kegiatan Tidak Boleh Kosong.',
			'namaPeminjam.required'=> 'Nama Peminjam Tidak Boleh Kosong.',
			'kontak.required'=> 'Kontak Peminjam Tidak Boleh Kosong.',
			'jamMulai.required'=> 'Jam Mulai Kegiatan Tidak Boleh Kosong.',
			'jamSelesai.required'=> 'Jam Selesai Kegiatan Tidak Boleh Kosong.',
        ];
    }
}
