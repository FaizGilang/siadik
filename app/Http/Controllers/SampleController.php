<?php

namespace App\Http\Controllers;

use App\CalendarEvent;
use App\Http\Requests;
use Carbon\Carbon;
use MaddHatter\LaravelFullcalendar\Calendar;
use MaddHatter\LaravelFullcalendar;
use MaddHatter\LaravelFullcalendar\Event;

class SampleController extends Controller
{

    /**
     * @var CalendarEvent
     */
    private $calendarEvent;

    /**
     * @param CalendarEvent $calendarEvent
     */
    public function __construct(CalendarEvent $calendarEvent)
    {
        $this->calendarEvent = $calendarEvent;
    }

    public function CalendarEvent()
    {
        

        $databaseEvents = $this->calendarEvent->all();

        $calendar = \Calendar::addEvents($databaseEvents);

        return view('calendar', compact('calendar'));
    }

}
