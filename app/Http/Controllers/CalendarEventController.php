<?php namespace App\Http\Controllers;

use App\CalendarEvent;
use App\Peminjaman;
use App\Http\Requests;
use App\Http\Requests\Calender\StoreRequest;
use App\Http\Requests\Calender\UpdateRequest;
use Illuminate\Http\Request;
use Redirect;
use \Input as Input;


class CalendarEventController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $calendar_events = CalendarEvent::all();

        return view('calendar_events.index', compact('calendar_events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('calendar_events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        $peminjamans = new Peminjaman();
        $peminjamans->namaRuang=$request->namaRuang;
        $peminjamans->tanggalPakai=$request->tanggalPakai;
        $peminjamans->kegiatan=$request->kegiatan;
        $peminjamans->namaPeminjam=$request->namaPeminjam;
        $peminjamans->kontak=$request->kontak;
        $peminjamans->jamMulai=$request->jamMulai;
        $peminjamans->jamSelesai=$request->jamSelesai;
		if($request->jamMulai>=$request->jamSelesai){
			return  redirect('/')->with('alert-success', 'Jam Mulai tidak dapat Lebih Besar dari Jam Selesai.')->with('tanggal', $peminjamans->tanggalPakai)->with('kegiatan', $peminjamans->kegiatan)->with('nama', $peminjamans->namaPeminjam)->with('kontak', $peminjamans->kontak)->with('mulai', $peminjamans->jamMulai)->with('selesai', $peminjamans->jamSelesai);
		}
		
        if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $peminjamans->lampiran = $file->getClientOriginalName();
		}
        $peminjamans->status=$request->status;
        $peminjamans->save();

        $calendar_event = new CalendarEvent();
        $calendar_event->peminjamanID     = $peminjamans->id;
        $calendar_event->title            = $request->kegiatan;
		$mulai							  = $request->tanggalPakai." ".$request->jamMulai.":00";
		$selesai						  = $request->tanggalPakai." ".$request->jamSelesai.":00";
        $calendar_event->start            = $mulai;
        $calendar_event->end              = $selesai;
        $calendar_event->is_all_day       = $request->input("is_all_day");
        if($request->namaRuang=='Ruang Sidang'){

			if($request->status=='Disetujui')
			{
				$calendar_event->background_color = '#B71C1C';
			}
			else
			{
				$calendar_event->background_color = '#EF9A9A';
			}
		}
		else{

			if($request->status=='Disetujui')
			{
				$calendar_event->background_color = ' #1B5E20';
			}
			else
			{
				$calendar_event->background_color = '#81C784';
			}
		}
        $calendar_event->save();

        return redirect('/')->with('message', 'Item created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $calendar_event = CalendarEvent::findOrFail($id);

        return view('calendar_events.show', compact('calendar_event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $calendar_event = CalendarEvent::findOrFail($id);

        return view('calendar_events.edit', compact('calendar_event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int    $id
     * @param Request $request
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $calendar_event = CalendarEvent::findOrFail($id);

        $calendar_event->title            = $request->input("title");
        $calendar_event->start            = $request->input("start");
        $calendar_event->end              = $request->input("end");
        $calendar_event->is_all_day       = $request->input("is_all_day");
        $calendar_event->background_color = $request->input("background_color");

        $calendar_event->save();

        return redirect()->route('calendar_events.index')->with('message', 'Item updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $calendar_event = CalendarEvent::findOrFail($id);
        $calendar_event->delete();

        return redirect()->route('calendar_events.index')->with('message', 'Item deleted successfully.');
    }

}
