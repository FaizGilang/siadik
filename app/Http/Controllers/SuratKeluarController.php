<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuratKeluar;
use App\Http\Requests;
use App\Http\Requests\SuratKeluar\StoreRequest;
use App\Http\Requests\SuratKeluar\UpdateRequest;
use App\Http\Requests\SuratMasuk\FilterRequest;
use DB;
use Redirect;
use \Input as Input;

class SuratKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewSuratKeluar()
    {
        $surat_keluars = SuratKeluar::all();
        return view('admin.suratkeluar.index', compact('surat_keluars'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchSuratKeluar(Request $request)
    {
        $surat_keluars = SuratKeluar::where('perihal', 'LIKE', '%'.$request->s.'%')->paginate(10)->appends(['s' => $request->s]);
        return view('admin.suratkeluar.index', compact('surat_keluars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.suratkeluar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createSuratKeluar(StoreRequest $request)
    {
        $surat_keluars = new SuratKeluar();
        $surat_keluars->nomor=$request->nomor;
        $surat_keluars->penerima=$request->penerima;
        $surat_keluars->tanggal=$request->tanggal;
        $surat_keluars->perihal=$request->perihal;

        if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $surat_keluars->gambar = $file->getClientOriginalName();
		}
		$surat_keluars->save();
        return  redirect('/admin/suratkeluar')->with('alert-success', 'Data Berhasil Ditambahkan.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $surat_keluars = SuratKeluar::findOrFail($id)	;
        return view('admin.suratkeluar.edit', compact('surat_keluars'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSuratKeluar(UpdateRequest $request)
    {
        $id = $request->id;
        $surat_keluars = SuratKeluar::findOrFail($id);
		$surat_keluars->nomor=$request->nomor;
		$surat_keluars->penerima=$request->penerima;
		$surat_keluars->tanggal=$request->tanggal;
		$surat_keluars->perihal=$request->perihal;
        if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $surat_keluars->gambar = $file->getClientOriginalName();
		}
        $surat_keluars->save();
	    return  redirect('/admin/suratkeluar')->with('alert-success', 'Data Berhasil Diubah.');
    }

    /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
    public function filterSuratKeluar(FilterRequest $request)
    {
        if ($request->bulan != '00'){
            $surat_keluars = SuratKeluar::whereMonth('tanggal', $request->bulan) -> whereYear('tanggal', $request->tahun)->paginate(10);
     		$surat_keluars->bulan = $request->bulan;
     		$surat_keluars->tahun = $request->tahun;
        }
        else {
            $surat_keluars = SuratKeluar::whereYear('tanggal', $request->tahun)->paginate(10);
            $surat_keluars->bulan = $request->bulan;
 		    $surat_keluars->tahun = $request->tahun;
        }
        return view('admin.suratkeluar.filter', compact('surat_keluars'));
    }

    /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
 	public function rekapitulasiSuratKeluar(Request $request)
     {
         if ($request->bulan != '00'){
            $surat_keluars = SuratKeluar::whereMonth('tanggal', $request->bulan) -> whereYear('tanggal', $request->tahun)->paginate(10);
      		$surat_keluars->bulan = $request->bulan;
      		$surat_keluars->tahun = $request->tahun;
         }
         else {
            $surat_keluars = SuratKeluar::whereYear('tanggal', $request->tahun)->paginate(10);
            $surat_keluars->bulan = $request->bulan;
  		    $surat_keluars->tahun = $request->tahun;
         }
         return view('admin.suratkeluar.rekapitulasi', compact('surat_keluars'));
     }
}
