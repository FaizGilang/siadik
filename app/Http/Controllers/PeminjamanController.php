<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use App\CalendarEvent;
use App\Http\Requests;
use App\Http\Requests\Peminjaman\StoreRequest;
use App\Http\Requests\Peminjaman\UpdateRequest;
use App\Http\Requests\SuratMasuk\FilterRequest;
use DB;
use Redirect;
use \Input as Input;
use Illuminate\Pagination\LengthAwarePaginator;


class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewPeminjaman($ruang)
    {
        if($ruang == 'ruangsidang'){
            $peminjamans = Peminjaman::where('namaRuang','=','Ruang Sidang')->simplePaginate(20);
            return view('admin.ruangsidang.index', compact('peminjamans'));
        }
        elseif($ruang == 'laboratorium') {
            $peminjamans = Peminjaman::where('namaRuang','=','Laboratorium')->simplePaginate(20);
            return view('admin.laboratorium.index', compact('peminjamans'));

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ruang)
    {
        if($ruang == 'ruangsidang'){
            return view('admin.ruangsidang.create');
        }
        elseif($ruang == 'laboratorium') {
            return view('admin.laboratorium.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addPeminjaman(StoreRequest $request)
    {
        $peminjamans = new Peminjaman();
        $peminjamans->namaRuang=$request->namaRuang;
        $peminjamans->tanggalPakai=$request->tanggalPakai;
        $peminjamans->kegiatan=$request->kegiatan;
        $peminjamans->namaPeminjam=$request->namaPeminjam;
        $peminjamans->kontak=$request->kontak;
        $peminjamans->jamMulai=$request->jamMulai;
        $peminjamans->jamSelesai=$request->jamSelesai;
		if($request->jamMulai>=$request->jamSelesai){
			if ($request->namaRuang == 'Laboratorium') {
				return  redirect('/admin/peminjaman/laboratorium/create')->with('alert-success', 'Jam Mulai tidak dapat Lebih Besar dari Jam Selesai.')->with('tanggal', $peminjamans->tanggalPakai)->with('kegiatan', $peminjamans->kegiatan)->with('nama', $peminjamans->namaPeminjam)->with('kontak', $peminjamans->kontak)->with('mulai', $peminjamans->jamMulai)->with('selesai', $peminjamans->jamSelesai);
			}
			elseif ($request->namaRuang == 'Ruang Sidang') {
				return  redirect('/admin/peminjaman/ruangsidang/create')->with('alert-success', 'Jam Mulai tidak dapat Lebih Besar dari Jam Selesai.')->with('tanggal', $peminjamans->tanggalPakai)->with('kegiatan', $peminjamans->kegiatan)->with('nama', $peminjamans->namaPeminjam)->with('kontak', $peminjamans->kontak)->with('mulai', $peminjamans->jamMulai)->with('selesai', $peminjamans->jamSelesai);
			}
		}
		/**if (Peminjaman::where($request->tanggalPakai,'=','tanggalPakai')->where($request->jamMulai,'=','jamMulai')->where($request->jamSelesai,'=','jamSelesai') != null){
			if($request->namaRuang == 'Ruang Sidang'){
				return redirect('/admin/peminjaman/ruangsidang/create');
			}
			elseif($request->namaRuang == 'Laboratorium') {
				$peminjamans = Peminjaman::where('namaRuang','=','Laboratorium')->simplePaginate(20);
				return view('admin.laboratorium.index', compact('peminjamans'));
			}
		}**/

        if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $peminjamans->lampiran = $file->getClientOriginalName();
		}
        $peminjamans->status=$request->status;
        $peminjamans->save();

        $calendar_event = new CalendarEvent();
        $calendar_event->peminjamanID     = $peminjamans->id;
        $calendar_event->title            = $request->kegiatan;
		$mulai							  = $request->tanggalPakai." ".$request->jamMulai.":00";
		$selesai						  = $request->tanggalPakai." ".$request->jamSelesai.":00";
        $calendar_event->start            = $mulai;
        $calendar_event->end              = $selesai;
        $calendar_event->is_all_day       = $request->input("is_all_day");
		if($request->namaRuang=='Ruang Sidang'){

			if($request->status=='Disetujui')
			{
				$calendar_event->background_color = '#81c784';
			}
			else
			{
				$calendar_event->background_color = '#4caf50';
			}
		}
		else{

			if($request->status=='Disetujui')
			{
				$calendar_event->background_color = ' #f44336';
			}
			else
			{
				$calendar_event->background_color = '#e57373';
			}
		}
        $calendar_event->save();

        if ($request->namaRuang == 'Laboratorium') {
            return  redirect('/admin/peminjaman/laboratorium')->with('alert-success', 'Data Berhasil Ditambahkan.');
        }
        elseif ($request->namaRuang == 'Ruang Sidang') {
            return  redirect('/admin/peminjaman/ruangsidang')->with('alert-success', 'Data Berhasil Ditambahkan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ruang,$id)
    {
        $peminjamans = Peminjaman::findOrFail($id);
        $peminjamans->jamMulai = substr($peminjamans->jamMulai, 0, -3);
        $peminjamans->jamSelesai = substr($peminjamans->jamSelesai, 0, -3);
        if($ruang == 'ruangsidang'){
            return view('admin.ruangsidang.edit', compact('peminjamans'));
        }
        elseif($ruang == 'laboratorium') {
            return view('admin.laboratorium.edit', compact('peminjamans'));

		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPeminjaman(UpdateRequest $request)
    {
        $id = $request->id;
        $peminjamans = Peminjaman::findOrFail($id);
        $peminjamans->namaRuang=$request->namaRuang;
        $peminjamans->tanggalPakai=$request->tanggalPakai;
        $peminjamans->kegiatan=$request->kegiatan;
        $peminjamans->namaPeminjam=$request->namaPeminjam;
        $peminjamans->kontak=$request->kontak;
        $peminjamans->jamMulai=$request->jamMulai;
        $peminjamans->jamSelesai=$request->jamSelesai;
		if($request->jamMulai>=$request->jamSelesai){
			if ($request->namaRuang == 'Laboratorium') {
				return  redirect('/admin/peminjaman/laboratorium/create')->with('alert-success', 'Jam Mulai tidak dapat Lebih Besar dari Jam Selesai.')->with('tanggal', $peminjamans->tanggalPakai)->with('kegiatan', $peminjamans->kegiatan)->with('nama', $peminjamans->namaPeminjam)->with('kontak', $peminjamans->kontak)->with('mulai', $peminjamans->jamMulai)->with('selesai', $peminjamans->jamSelesai);
			}
			elseif ($request->namaRuang == 'Ruang Sidang') {
				return  redirect('/admin/peminjaman/ruangsidang/create')->with('alert-success', 'Jam Mulai tidak dapat Lebih Besar dari Jam Selesai.')->with('tanggal', $peminjamans->tanggalPakai)->with('kegiatan', $peminjamans->kegiatan)->with('nama', $peminjamans->namaPeminjam)->with('kontak', $peminjamans->kontak)->with('mulai', $peminjamans->jamMulai)->with('selesai', $peminjamans->jamSelesai);
			}
		}
		
        if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $peminjamans->lampiran = $file->getClientOriginalName();
		}
        $peminjamans->status=$request->status;
        $peminjamans->save();

        $peminjamanid = $request->id;
        $calendar_event = CalendarEvent::where('peminjamanid', '=' , $peminjamanid)->firstOrFail();
        $calendar_event->title            = $request->kegiatan;
		$mulai							  = $request->tanggalPakai." ".$request->jamMulai.":00";
		$selesai						  = $request->tanggalPakai." ".$request->jamSelesai.":00";
        $calendar_event->start            = $mulai;
        $calendar_event->end              = $selesai;
        $calendar_event->is_all_day       = $request->input("is_all_day");

		if($request->namaRuang=='Ruang Sidang'){

			if($request->status=='Disetujui')
			{
				$calendar_event->background_color = '#B71C1C';
			}
			else
			{
				$calendar_event->background_color = '#EF9A9A';
			}
		}
		else{
			if($request->status=='Disetujui')
			{
				$calendar_event->background_color = ' #1B5E20';
			}
			else
			{
				$calendar_event->background_color = '#81C784';
			}
		}
        $calendar_event->save();

        if ($request->namaRuang == 'Laboratorium') {
            return  redirect('/admin/peminjaman/laboratorium')->with('alert-success', 'Data Berhasil Diubah.');
        }
        elseif ($request->namaRuang == 'Ruang Sidang') {
            return  redirect('/admin/peminjaman/ruangsidang')->with('alert-success', 'Data Berhasil Diubah.');
        }
    }

    /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchPeminjaman(Request $request, $ruang)
    {
        //$search = Request::get('s');
        if($ruang == 'ruangsidang'){
            $peminjamans = Peminjaman::where('namaRuang','=','Ruang Sidang')->where('kegiatan','like','%'.$request->s.'%')->orderBy('id')->paginate(10);
            return view('admin.ruangsidang.index', compact('peminjamans'));
        }
        elseif($ruang == 'laboratorium') {
            $peminjamans = Peminjaman::where('namaRuang','=','Laboratorium')->where('kegiatan','like','%'.$request->s.'%')->orderBy('id')->paginate(10);
            return view('admin.laboratorium.index', compact('peminjamans'));
        }
    }

    /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
    public function filterPeminjaman(FilterRequest $request, $ruang)
    {
        if ($request->ruang == 'laboratorium') {
            if ($request->bulan != '00'){
                $peminjamans = Peminjaman::where('namaRuang','=','Laboratorium')->whereMonth('tanggalPakai', $request->bulan) -> whereYear('tanggalPakai', $request->tahun)->paginate(10);
         		$peminjamans->bulan = $request->bulan;
         		$peminjamans->tahun = $request->tahun;
            }
            else {
                $peminjamans = Peminjaman::where('namaRuang','=','Laboratorium') -> whereYear('tanggalPakai', $request->tahun)->paginate(10);
                $peminjamans->bulan = $request->bulan;
         		$peminjamans->tahun = $request->tahun;
            }
            return view('admin.laboratorium.filter', compact('peminjamans'));
        }
        elseif ($request->ruang == 'ruangsidang') {
            if ($request->bulan != '00'){
                $peminjamans = Peminjaman::where('namaRuang','=','Ruang Sidang')->whereMonth('tanggalPakai', $request->bulan) -> whereYear('tanggalPakai', $request->tahun)->paginate(10);
         		$peminjamans->bulan = $request->bulan;
         		$peminjamans->tahun = $request->tahun;
            }
            else {
                $peminjamans = Peminjaman::where('namaRuang','=','Ruang Sidang') -> whereYear('tanggalPakai', $request->tahun)->paginate(10);
                $peminjamans->bulan = $request->bulan;
         		$peminjamans->tahun = $request->tahun;
            }
            return view('admin.ruangsidang.filter', compact('peminjamans'));
        }
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function rekapitulasiPeminjaman(Request $request, $ruang)
    {
        if ($request->ruang == 'laboratorium') {
            if ($request->bulan != '00'){
                $peminjamans = Peminjaman::where('namaRuang','=','Laboratorium')->whereMonth('tanggalPakai', $request->bulan) -> whereYear('tanggalPakai', $request->tahun)->paginate(10);
         		$peminjamans->bulan = $request->bulan;
         		$peminjamans->tahun = $request->tahun;
            }
            else {
                $peminjamans = Peminjaman::where('namaRuang','=','Laboratorium') -> whereYear('tanggalPakai', $request->tahun)->paginate(10);
                $peminjamans->bulan = $request->bulan;
         		$peminjamans->tahun = $request->tahun;
            }
            return view('admin.laboratorium.rekapitulasi', compact('peminjamans'));
        }
        elseif ($request->ruang == 'ruangsidang') {
            if ($request->bulan != '00'){
                $peminjamans = Peminjaman::where('namaRuang','=','Ruang Sidang')->whereMonth('tanggalPakai', $request->bulan) -> whereYear('tanggalPakai', $request->tahun)->paginate(10);
         		$peminjamans->bulan = $request->bulan;
         		$peminjamans->tahun = $request->tahun;
            }
            else {
                $peminjamans = Peminjaman::where('namaRuang','=','Ruang Sidang') -> whereYear('tanggalPakai', $request->tahun)->paginate(10);
                $peminjamans->bulan = $request->bulan;
         		$peminjamans->tahun = $request->tahun;
            }
            return view('admin.ruangsidang.rekapitulasi', compact('peminjamans'));
        }
    }

    /**
     * Display approve peminjaman.
     *
     * @return \Illuminate\Http\Response
     */
    public function approvePeminjaman($ruang, $id)
    {
        $peminjamans = Peminjaman::findOrFail($id);
		$peminjamanid = $id;
        $calendar_event = CalendarEvent::where('peminjamanid', '=' , $peminjamanid)->firstOrFail();




        if($ruang == 'ruangsidang'){
            $peminjamans->status = 'Disetujui';
			$calendar_event->background_color = '#B71C1C';
            $calendar_event->save();
            $peminjamans->save();
            return  redirect('/admin/peminjaman/ruangsidang')->with('alert-success', 'Peminjaman Berhasil Disetujui.');
        }
        elseif($ruang == 'laboratorium') {
            $peminjamans->status = 'Disetujui';
			$calendar_event->background_color = '#1B5E20';
            $peminjamans->save();
            $calendar_event->save();
            return  redirect('/admin/peminjaman/laboratorium')->with('alert-success', 'Peminjaman Berhasil Disetujui.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancelPeminjaman($ruang, $id)
    {
        $peminjamans = Peminjaman::findOrFail($id);
        $calendar_event = CalendarEvent::where('peminjamanid', '=' , $id)->firstOrFail();
        if($ruang == 'ruangsidang'){
            $peminjamans->delete();
            $calendar_event->delete();
            return  redirect('/admin/peminjaman/ruangsidang')->with('alert-success', 'Peminjaman Berhasil Dibatalkan.');
        }
        elseif($ruang == 'laboratorium') {
            $peminjamans->delete();
            $calendar_event->delete();
            return  redirect('/admin/peminjaman/laboratorium')->with('alert-success', 'Peminjaman Berhasil Dibatalkan.');
        }

    }
}
