<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', ['uses' => 'SampleController@CalendarEvent']);

Route::resource('calendar_events', 'CalendarEventController');
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware'=>['auth']], function (){


/* Surat Masuk */
	Route::get('/admin', 'SuratMasukController@viewSuratMasuk');
	//index
    Route::get('/admin/suratmasuk', 'SuratMasukController@viewSuratMasuk');
	//create
	Route::get('/admin/suratmasuk/create', 'SuratMasukController@create');
    Route::post('/admin/suratmasuk/store', 'SuratMasukController@createSuratMasuk');
	//edit
	Route::get('/admin/suratmasuk/edit/{id}', 'SuratMasukController@edit');
    Route::post('/admin/suratmasuk/update/', 'SuratMasukController@updateSuratMasuk');
    //delete
    Route::get('/admin/suratmasuk/delete/{id}', 'SuratMasukController@deleteSuratMasuk');
    //search
    Route::get('/admin/suratmasuk/search', 'SuratMasukController@searchSuratMasuk');
	//rekap
	Route::get('/admin/suratmasuk/rekap', 'SuratMasukController@rekapitulasiSuratMasuk');
	//filter
    Route::get('/admin/suratmasuk/filter', 'SuratMasukController@filterSuratMasuk');

/* Surat Keluar */
    //index
    Route::get('/admin/suratkeluar', 'SuratKeluarController@viewSuratKeluar');
    //create
    Route::get('/admin/suratkeluar/create', 'SuratKeluarController@create');
    Route::post('/admin/suratkeluar/store', 'SuratKeluarController@createSuratKeluar');
    //edit
    Route::get('/admin/suratkeluar/edit/{id}', 'SuratKeluarController@edit');
    Route::post('/admin/suratkeluar/update/', 'SuratKeluarController@updateSuratKeluar');
    //delete
    Route::get('/admin/suratkeluar/delete/{id}', 'SuratKeluarController@deleteSuratKeluar');
    //search
    Route::get('/admin/suratkeluar/search', 'SuratKeluarController@searchSuratKeluar');
    //rekap
	Route::get('/admin/suratkeluar/rekap', 'SuratKeluarController@rekapitulasiSuratKeluar');
    //filter
    Route::get('/admin/suratkeluar/filter', 'SuratKeluarController@filterSuratKeluar');

/* Peminjaman Ruang */
    //index
    Route::get('/admin/peminjaman/{ruang}', 'PeminjamanController@viewPeminjaman');
    //create
    Route::get('/admin/peminjaman/{ruang}/create', 'PeminjamanController@create');
    Route::post('/admin/peminjaman/store', 'PeminjamanController@addPeminjaman');
    //edit
    Route::get('/admin/peminjaman/{ruang}/edit/{id}', 'PeminjamanController@edit');
    Route::post('/admin/peminjaman/update/', 'PeminjamanController@editPeminjaman');
    //delete
    Route::get('/admin/peminjaman/{ruang}/delete/{id}', 'PeminjamanController@cancelPeminjaman');
    //search
    Route::get('/admin/peminjaman/{ruang}/search', 'PeminjamanController@searchPeminjaman');
    //approve
    Route::get('/admin/peminjaman/{ruang}/approve/{id}', 'PeminjamanController@approvePeminjaman');
    //rekap
    Route::get('/admin/peminjaman/{ruang}/rekap', 'PeminjamanController@rekapitulasiPeminjaman');
	//filter
    Route::get('/admin/peminjaman/{ruang}/filter', 'PeminjamanController@filterPeminjaman');

});
